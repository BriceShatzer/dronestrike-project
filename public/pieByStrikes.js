function flagBackground(targetSVG,imgURL,name){
    var defs;    
    var targetSVG = d3.select(targetSVG);
    var backgroundScaling

    if(targetSVG.select('defs').empty()){//if if image doesn't have the defs element
        defs = targetSVG.append('defs');
    } else {
        defs = targetSVG.select('defs');
    }

    if(targetSVG.attr('width') <= 480 || targetSVG.attr('height') <= 320 ){
        backgroundScaling = 1
    } else if(targetSVG.attr('width') <= 960 || targetSVG.attr('height') <= 640){
        backgroundScaling = 2
    } else{
        backgroundScaling = 3
    }

    defs.append('pattern')
        .attr('id', encodeURIComponent(name))
            .attr('width', 100 / (targetSVG.attr('width') / (120*backgroundScaling) ) + '%' )
            .attr('height', 100 / (targetSVG.attr('height') / (80*backgroundScaling) ) +'%' )
            .attr('patternUnits', 'userSpaceOnUse')
        .append('image')
            .attr('xlink:href',imgURL)
            .attr('patternUnits','userSpaceOnUse')
            .attr('width', (120*backgroundScaling)-1 )
            .attr('height', (80*backgroundScaling)-1 );  
}


function pieByStrikes(chartableData) {
    d3.select('#pieChart>svg').remove();//remove existing chart if there 
    var pieChart = d3.select('#pieChart');
    
    var width, height;
    width = height = Math.min( 
        pieChart.node().scrollWidth, 
        (d3.select('#tab1').node().scrollHeight - pieChart.node().scrollHeight) - 25 //25 = arbitrary number to prevent vert scrolling
    );
    //using the containing elements viewport dependent width to determine the chart's width & height
   
    var radius = width / 2;

    var svg = d3.select('#pieChart').append('svg')
        .data([chartableData])
            .attr('width', width)
            .attr('height', height)
        .append('g')
            .attr('transform','translate('+ (width/2) + ',' + (height/2) + ')');   

    var paths = svg.selectAll('g.slice')
        .data(
            d3.layout.pie()
                .value( function(d){return d.strikes; })
                .sort(null)
        )
        .enter()
            .append('svg:g')
                .attr('class','slice');

    paths.append('path')
        .attr('d', d3.svg.arc().outerRadius(radius) )
        .attr('fill', function(d){
            var label = d.data.label;        
            var fillName = label.replace(/[\s!\"#$%&'\(\)\*\+,\.\/:;<=>\?\@\[\\\]\^`\{\|\}~]/g, '')+'Flag';//create valid class name
            flagBackground( this.ownerSVGElement, nationalIdentity[label].flag, fillName);
            return 'url(#'+fillName+')';
        })

    //tootip functionality 
    paths.on('mouseover', function(d) { 
        var total = d3.sum (chartableData.map(function(d){
            return d.strikes;
        }));
        var percent = Math.round(1000 * d.data.strikes / total) / 10;

        tooltip.select('.flag > img')
            .attr('src' , nationalIdentity[d.data.label].flag)
            .attr('alt', d.data.label)
        tooltip.select('.label').html(d.data.label);
        tooltip.select('.strikes').html(d.data.strikes); 
        tooltip.select('.percent').html(percent + '%'); 
        tooltip.style('position', 'absolute')
        tooltip.style('display', 'block');
    });

    paths.on('mousemove', function(){
        var x = d3.event.clientX;
        var y = d3.event.clientY;
        tooltip.style('top', (y - 20) + 'px');
        tooltip.style('left', (x + 20) + 'px');
    });

    paths.on('mouseout', function(d) {  
        tooltip.style('display', 'none');                       
    });

    var tooltip = d3.select('#pieChartInfo')
        .append('div')
        .attr('class', 'tooltip');      

    tooltip.append('div')
        .attr('class','flag')
        .html('<img />');

    tooltip.append('div')
      .attr('class', 'label');

    tooltip.append('div')
      .attr('class', 'strikes');

    tooltip.append('div')
      .attr('class', 'percent');
                  
}