
var people_path_d = "m0.420916,61.236355l0,-27.198532q0,-6.944302 4.629539,-9.451969q1.736075,-0.771597 4.050845,-0.771597l20.447122,0q2.314766,0 4.243746,0.771597q5.786915,2.893459 5.786915,9.451969l0,27.198532q-0.385799,3.086361 -2.989853,2.797062t-2.797062,-2.797062l0,-25.848267q-0.192905,-0.964478 -1.73608,-0.771572t-1.543179,0.771572l0,59.219494q-0.385796,2.700562 -2.507669,3.568558t-4.147238,-0.385796t-2.025377,-3.375748l0,-36.071812q-0.385799,-1.736073 -2.121874,-1.446777t-1.736074,1.446777l0,36.071812q-0.192892,2.893456 -2.411263,3.761543t-4.147251,-0.289284t-2.121868,-3.279274l0,-59.219494q-0.385792,-0.964478 -1.736075,-0.771572t-1.54318,0.771572l0,25.848267q-0.385793,3.086361 -2.989856,2.797062t-2.604167,-2.797062l-0.0001,0zm10.030763,-49.864038q0,-3.954443 2.797056,-6.751397t6.751415,-2.797061t6.751408,2.797061t2.797064,6.751397t-2.797064,6.751418t-6.751408,2.797062t-6.751415,-2.797062t-2.797056,-6.751418z";

function countryDeathToll(country) {
	var incidents = _.where(strikes,{country:country});

	var countTotals  = {
		likely_deaths : 0,
		deaths_min : 0,
		deaths_max : 0,
		child_deaths : 0,
		injuries : 0,
	}
	_.each(incidents, function(obj,i){
		var childrenInt;
		var injuriesInt; 
		var minInt;
		var maxInt;

		//data cleanup
		if( childrenInt = parseInt(obj.children) ){} else { childrenInt = 0; }
		if( injuriesInt = parseInt(obj.injuries) ){} else { injuriesInt = 0; }
		if( minInt = parseInt(obj.deaths_min) ){} else { minInt = 0; }
		if( maxInt = parseInt(obj.deaths_max) ){} else { maxInt = 0; }

		countTotals.deaths_min += minInt;
		countTotals.deaths_max += maxInt;
		countTotals.child_deaths += childrenInt;
		countTotals.injuries += injuriesInt;

		countTotals.likely_deaths += Math.round( (minInt+maxInt)/2 );
	});

	//quick data integrity check
	if( 
		countTotals.deaths_min > countTotals.likely_deaths || 
		countTotals.likely_deaths > countTotals.deaths_max ||
		countTotals.deaths_min > countTotals.deaths_max
	){
		pageError('Weird country data issue. Verify the information that is being parsed is valid.')
		return 

	}

	return countTotals;
}

function makeHeader(target, country, deaths, childDeaths ){
	target.append('div')
		.attr('class', 'countryHeader');

	var title = (country.indexOf(' border') === -1)
	var container = d3.select('.countryHeader');	

	container.append('h1')
		.text('Dronestrike Deaths '+ (country.indexOf(' Border') === -1 ? 'in ' :  'on the ')+country)

	container.append('h3')
		.text('Approximate Total Deaths: '+deaths)

	container.append('h3')
		.text('Number of Children Killed: '+childDeaths)
}

function makeStickAdults(target,count,color){
	for (; count > 0; count--) {
		target.append('svg')
			.attr('height',50)
			.attr('viewBox', '0 0 39.6 100')
			.append('path')
				.attr('d', people_path_d)
				.attr('fill', color);
	};
}

function makeStickAdultChildren(target,count,color) {
		for (; count > 0; count--) {
		target.append('svg')
			.attr('height',30)
			.attr('viewBox', '0 0 39.6 100')
			.append('path')
				.attr('d', people_path_d)
				.attr('fill', color);
	};
}

function buildCountrySelector(countyList){
	d3.selectAll('#countrySelector>div').remove();
	d3.selectAll('#deathToll>div').remove();//remove existing chart if there 

	var countrySelector = d3.select('#countrySelector');
	var totalDeaths = 0;
	var totalChildDeaths = 0;
	var width  = countrySelector.node().scrollWidth*(1/(countyList.length+1));

	_.each( countyList, function(c,i) {
		var count = countryDeathToll(c);
		var adults = count.likely_deaths;
		var children = count.child_deaths;

		totalDeaths += adults;
		totalChildDeaths += children

		countrySelector
			.append('div')
			.attr('style', 'width:'+width+'px')
			.on('click',function(){
				var deathToll_div = d3.select('#deathToll');

				deathToll_div.classed('hidden',true);
				d3.select('#loading').classed('hidden',false);

				d3.selectAll('#deathToll>div').remove();

				makeHeader(
					deathToll_div, 
					c, 
					adults, 
					children
				);

				var container = deathToll_div.append('div')
					.attr('class','people');

				makeStickAdultChildren(
					container,
					children,
					nationalIdentity[c].color
				);

				makeStickAdults(
					container,
					adults-children,
					nationalIdentity[c].color
				);

				d3.select('#loading').classed('hidden',true);
				deathToll_div.classed('hidden',false);				
			})
			.html('<h3>'+c+'</h3>')
			.insert('img',':before')
				.attr('src', nationalIdentity[c].flag);

		if( (i+1) === countyList.length ){
			d3.select('#deathToll').append('div')
				.attr('class','startHeader')
				.html('<h2>This site contains data pertaining to the drone strikes that have killed <strong>'+totalDeaths+'</strong> people,<br> including <strong>'+totalChildDeaths+'</strong> children.</h2> <h1>Select a location to see specific details.</h1>');
		}
			
	});
		
}

