var data;
var strikes;
var tracked_countries;
var chartingData_strikesPerCountry;
var nationalIdentity = {
    'Pakistan':{
        'flag':'/assets/Flag_of_Pakistan.svg',
        'color':'#01411C'
    },
    'Yemen':{
        'flag':'/assets/Flag_of_Yemen.svg',
        'color':'#CE1126'
    },
    'Somalia':{
        'flag':'/assets/Flag_of_Somalia.svg',
        'color':'#4189DD'
    },
    'Pakistan-Afghanistan Border':{
        'flag':'/assets/Flag_of_Afghanistan.svg',
        'color':'#009900'
    }
}

function pageError(errorMessage){
    var body = d3.select('body');
    body.append('div')
        .attr ('class','errorOverlay')
                .append('div')
                    .attr('class','errorImage')
                        .append('img')
                        .attr('src','/assets/error.svg');
    body.select('.errorOverlay')
                    .append('h3')
                        .attr('class','errorText')
                        .text(errorMessage);

    console.log(errorMessage);
}

//===load the data, then do some stuff 
d3.json('/data.json',function(error,json){
    if(error){
        pageError(error.responseText);
        return
    }

    data = json;
    strikes = data.strike;
    tracked_countries = _.countBy(strikes, 'country'); 

    function openTab(d3SelectedTab){
    //returns false if tab is already active & true if targeted tab becomes active

        if( d3SelectedTab.classed('active') ){
            //clicked tab is currently active
            return false 
        } else {
            //hide active tab
            d3.select('.tab.active')
                .classed('active', false);
            
            //set active tab
            d3SelectedTab.classed('active', true);
            
            return true
        }
    }
    function tabBarManagment(tabControl) {
        d3.select('.tabBar>.active').classed('active',false);
        d3.select(tabControl).classed('active',true);
    }

    //-- info page
    d3.select('#about').on('click', function(event) {
        var tab = d3.select('#tab0');
        if( openTab(tab) ){
            tabBarManagment(this);

        }
    });

    //-- make me a pie
    d3.select('#pie').on('click', function(event) {
        var tab = d3.select('#tab1');
        if( openTab(tab) ){
            tabBarManagment(this);

            d3.select('#pieChart')
            .html('<h4>This site contains data pertaining to the drone strike incidents that have taken place in <strong>'+ _.keys(tracked_countries).length+'</strong> different countries/regions.<br> Hover over the map to see details.</h4')

            //create charting data
            chartingData_strikesPerCountry = [];
                _.forEach(
                    _.keys(tracked_countries), function (c,i) {
                        var obj = {}
                        obj.label = c;
                        obj.strikes = tracked_countries[c];
                        chartingData_strikesPerCountry.push(obj);       
                });

            //build pieChart
            pieByStrikes(chartingData_strikesPerCountry);

            //rebuild things when the window is resized;
            d3.select(window).on('resize', function(){
                if(d3.select('#tab1').classed('active')){
                    pieByStrikes(chartingData_strikesPerCountry);
                }
            });
        }      
    });

    //-- make stick people 
    d3.select('#countries').on('click', function(event){
        var tab = d3.select('#tab2');
        if( openTab(tab) ){
            tabBarManagment(this);
            buildCountrySelector( _.keys(tracked_countries) );
        }
    });

});




    







