#Dronestrike-project

To get the project up in running:

- clone the repo
- from the cloned repo's directory, run `npm install` and then `npm start`
- this will serve the page on `http://localhost:8080/`

--- 

Having had some experience with lodash & underscore, I was reasonably comfortable with parsing and manipulating datasets but I haven't really had much experience building visualizations out of them. I wanted to use this project as a means to rectify that, with one of my main goals being to get comfortable with D3. 

While the functionality itself is relatively sparse, I feel like I was able to get a really good handle on the library.

Given more time I would: 

- provide more interactivity (filter by time, geo-fence, etc )
- add a build process with grunt or gulp (concatenate/minify everything, switch to sass, test stuff, etc)
- actually style it