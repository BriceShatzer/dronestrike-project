var finalhandler = require('finalhandler');
var http = require('http');
var serveStatic = require('serve-static');
var request = require('request');
var fs = require('fs');

var serve = serveStatic('public');

request('http://api.dronestre.am/data').pipe(fs.createWriteStream('public/data.json'));
//request.get('http://api.dronestre.am/data').pipe(request.put('localhost:8080/livedata.json'));

var server = http.createServer(function(req, res){
	var done = finalhandler(req, res)
	serve(req, res, done)
})

server.listen(8080)